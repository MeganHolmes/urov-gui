

# System Imports
import tkinter as tk
from tkinter import END, ttk

# Project Imports
from structures import Rudder_Drive_Direction, Bouyancy_Direction, States
from structures import Connection_Status


class GUI():
    # Options
    in_use_demo = True
    inital_status_text = "UNKNOWN"
    inital_values = 0.0
    rudder_max_angle = 45
    elevator_max_angle = 45
    degree_symbol = u'\u00b0'

    # Constants
    label_col = 0
    status_col = 1
    control_col = 2
    bouyancy_col = 3
    slider_left_lbl_col = 0
    slider_col = 1
    slider_right_lbl_col = 2
    set_col = 3
    individual_stop_button_col = set_col + 1
    sensor_vals_col = 2

    win = None

    @classmethod
    def init_gui(self):

        self.win = tk.Tk()

        self.win.title("Underwater Remotely Operated Vehicle Command Window")

        # Change the main window icon
        self.win.iconbitmap('@WWR_logo.xbm')  # Has to be .xbm for linux,
        # use .ico for Windows

        # Prevent the window from being resized
        self.win.resizable(False, False)

        # Handle when the user closes the window
        self.win.protocol("WM_DELETE_WINDOW", self.close)

        self.create_styles()
        self.init_motor_elements()
        self.init_sensor_elements()
        self.init_auxilliary_elements()

    @classmethod
    def start(self, commands):
        if self.win is None:
            self.init_gui()

        self.commands = commands

        self.win.mainloop()

    @classmethod
    def create_styles(self):
        stop_style = ttk.Style()
        stop_style.configure('stop.TButton', background="red")

        error_style = ttk.Style()
        error_style.configure('error.TLabel', foreground="red")

        ok_style = ttk.Style()
        ok_style.configure('ok.TLabel', foreground="green")

        standard_style = ttk.Style()
        standard_style.configure('standard.TLabel', foreground="black")

        warning_style = ttk.Style()
        warning_style.configure('warning.TLabel', foreground="yellow")

    @classmethod
    def init_motor_elements(self):

        # Create the overall motor frame
        motor_frame = ttk.LabelFrame(self.win, text='Motors')
        motor_frame.grid(column=self.label_col, row=0)

        # Create the stop all motors button
        stop_all_motors_button = ttk.Button(
            motor_frame, text="Stop All Motors", command=self.stop_all_motors,
            style='stop.TButton')
        stop_all_motors_button.grid(column=2, row=0, sticky=tk.W)

        # Create all other elements
        self.create_motor_labels(motor_frame)
        self.create_motor_status(motor_frame)
        self.create_motor_controls(motor_frame)
        self.create_motor_bouyancy(motor_frame)

    @classmethod
    def init_sensor_elements(self):
        sensor_frame = ttk.LabelFrame(self.win, text="Sensors")
        sensor_frame.grid(column=self.label_col, row=1)

        self.create_sensor_labels(sensor_frame)
        self.create_sensor_status(sensor_frame)
        self.create_sensor_values(sensor_frame)

    @classmethod
    def init_auxilliary_elements(self):
        auxilliary_frame = ttk.LabelFrame(self.win, text="Auxilliary")
        auxilliary_frame.grid(column=self.label_col, row=2)

        ttk.Label(auxilliary_frame, text="Connection:").grid(
            column=self.label_col, row=0, sticky=tk.W)

        self.connection_status = ttk.Label(
            auxilliary_frame, text="DISCONNECTED", style='error.TLabel')
        self.connection_status.grid(column=self.status_col, row=0, sticky=tk.W)

    @classmethod
    def create_motor_labels(self, motor_frame):
        motor_label_frame = ttk.LabelFrame(motor_frame, text='Motor')
        motor_label_frame.grid(column=self.label_col, row=1)

        # Populate the labels
        self.thruster_label = ttk.Label(motor_label_frame, text="Thruster")
        self.thruster_label.grid(column=0, row=0, sticky=tk.W)
        self.elevator_label = ttk.Label(motor_label_frame, text="Elevator")
        self.elevator_label.grid(column=0, row=1, sticky=tk.W)
        self.rudder_label = ttk.Label(motor_label_frame, text="Rudder")
        self.rudder_label.grid(column=0, row=2, sticky=tk.W,)

    @classmethod
    def create_motor_status(self, motor_frame):
        motor_status_frame = ttk.LabelFrame(motor_frame, text='Status')
        motor_status_frame.grid(column=self.status_col, row=1)

        # Populate the inital status
        self.thruster_status = ttk.Label(
            motor_status_frame, text=self.inital_status_text,
            style='warning.TLabel')
        self.thruster_status.grid(column=0, row=0, sticky=tk.W)

        self.elevator_status = ttk.Label(
            motor_status_frame, text=self.inital_status_text,
            style='warning.TLabel')
        self.elevator_status.grid(column=0, row=1, sticky=tk.W)

        self.rudder_status = ttk.Label(
            motor_status_frame, text=self.inital_status_text,
            style='warning.TLabel')
        self.rudder_status.grid(column=0, row=2, sticky=tk.W)

    @classmethod
    def create_motor_controls(self, motor_frame):
        motor_control_frame = ttk.LabelFrame(motor_frame, text='Control')
        motor_control_frame.grid(column=self.control_col, row=1)

        # Create the left and right labels
        ttk.Label(motor_control_frame, text="-100%").grid(
            column=self.slider_left_lbl_col, row=0, sticky=tk.W)
        ttk.Label(motor_control_frame, text="100%").grid(
            column=self.slider_right_lbl_col, row=0, sticky=tk.W)

        ttk.Label(motor_control_frame,
                  text=str(-self.elevator_max_angle)+self.degree_symbol).grid(
            column=self.slider_left_lbl_col, row=1, sticky=tk.W)
        ttk.Label(motor_control_frame,
                  text=str(self.elevator_max_angle)+self.degree_symbol).grid(
            column=self.slider_right_lbl_col, row=1, sticky=tk.W)

        ttk.Label(motor_control_frame,
                  text=str(-self.rudder_max_angle)+self.degree_symbol).grid(
            column=self.slider_left_lbl_col, row=2, sticky=tk.W)
        ttk.Label(motor_control_frame,
                  text=str(self.rudder_max_angle)+self.degree_symbol).grid(
            column=self.slider_right_lbl_col, row=2, sticky=tk.W)

        # Create the sliders
        self.thruster_slider = ttk.Scale(
            motor_control_frame,
            from_=-100, to=100, orient=tk.HORIZONTAL,
            command=self.thruster_slider)
        self.thruster_slider.grid(column=self.slider_col, row=0, sticky=tk.W)

        self.elevator_slider = ttk.Scale(
            motor_control_frame,
            from_=-self.rudder_max_angle,
            to=self.rudder_max_angle,
            orient=tk.HORIZONTAL, command=self.elevator_slider)
        self.elevator_slider.grid(column=self.slider_col, row=1, sticky=tk.W)

        self.rudder_slider = ttk.Scale(
            motor_control_frame,
            from_=-self.rudder_max_angle,
            to=self.rudder_max_angle,
            orient=tk.HORIZONTAL, command=self.rudder_slider)
        self.rudder_slider.grid(column=self.slider_col, row=2, sticky=tk.W)

        # Create the set boxes
        self.thruster_set = ttk.Entry(motor_control_frame, width=5,
                                      justify=tk.CENTER)
        self.thruster_set.bind("<FocusOut>", self.thruster_set_change)
        self.thruster_set.grid(column=self.set_col, row=0, sticky=tk.W)
        self.elevator_set = ttk.Entry(motor_control_frame, width=5,
                                     justify=tk.CENTER)
        self.elevator_set.bind("<FocusOut>", self.elevator_set_change)
        self.elevator_set.grid(column=self.set_col, row=1, sticky=tk.W)
        self.rudder_set = ttk.Entry(motor_control_frame, width=5,
                                    justify=tk.CENTER)
        self.rudder_set.bind("<FocusOut>", self.rudder_set_change)
        self.rudder_set.grid(column=self.set_col, row=2, sticky=tk.W)

        # Initalize to zero
        self.thruster_set.insert(0, self.inital_values)
        self.elevator_set.insert(0, self.inital_values)
        self.rudder_set.insert(0, self.inital_values)

        # Create the individual stop buttons
        tk.Button(
            motor_control_frame,
            text="Stop", command=self.stop_thruster).grid(
            column=self.individual_stop_button_col,
            row=0, sticky=tk.W)
        tk.Button(
            motor_control_frame,
            text="Stop", command=self.stop_elevator).grid(
            column=self.individual_stop_button_col,
            row=1, sticky=tk.W)
        tk.Button(
            motor_control_frame,
            text="Stop", command=self.stop_rudder).grid(
            column=self.individual_stop_button_col,
            row=2, sticky=tk.W)

        # Create the rudder toggle buttons
        ttk.Label(
            motor_control_frame,
            text="Rudder Drive:").grid(
            column=0, row=3, sticky=tk.W)

        self.rudder_diff_button = tk.Button(
            motor_control_frame, text="Differential", command=self.rudder_diff)
        self.rudder_diff_button.grid(column=1, row=3, sticky=tk.W)

        self.rudder_together_button = tk.Button(
            motor_control_frame, text="Together", command=self.rudder_together)
        self.rudder_together_button.config(relief='sunken')
        self.rudder_together_button.grid(column=2, row=3, sticky=tk.W)

    @classmethod
    def create_motor_bouyancy(self, motor_frame):
        bouyancy_control_frame = ttk.LabelFrame(
            motor_frame,
            text='Bouyancy Control')
        bouyancy_control_frame.grid(column=self.bouyancy_col, row=1)

        # Create the estimated volume label
        ttk.Label(
            bouyancy_control_frame,
            text="Estimated Volume:").grid(
            column=1, row=0, sticky=tk.W)

        # Create the estimated volume entry

        self.estimated_volume_entry = ttk.Label(
            bouyancy_control_frame,
            text=str(self.inital_values) + " %")
        self.estimated_volume_entry.grid(column=1, row=1, sticky=tk.W)

        # Create the buttons
        self.bouyancy_up_button = tk.Button(bouyancy_control_frame,
                                            text="UP",
                                            command=self.bouyancy_up)
        self.bouyancy_up_button.grid(column=0, row=2, sticky=tk.W)

        self.bouyancy_hold_button = tk.Button(bouyancy_control_frame,
                                              text="HOLD",
                                              relief='sunken',
                                              command=self.bouyancy_hold)
        self.bouyancy_hold_button.grid(column=1, row=2, sticky=tk.W)

        self.bouyancy_down_button = tk.Button(bouyancy_control_frame,
                                              text="DOWN",
                                              command=self.bouyancy_down)
        self.bouyancy_down_button.grid(column=2, row=2, sticky=tk.W)

    @classmethod
    def create_sensor_labels(self, sensor_frame):
        sensor_label_frame = ttk.LabelFrame(sensor_frame, text="Sensor")
        sensor_label_frame.grid(column=0, row=0)

        # Populate the labels
        row_num = 0

        self.abs_pressure_label = ttk.Label(sensor_label_frame,
                                            text="Absolute Sensor Pressure")
        self.abs_pressure_label.grid(
            column=self.label_col, row=row_num, sticky=tk.W)
        row_num += 1

        self.diff_pressure_label = ttk.Label(
            sensor_label_frame,
            text="Differential Sensor Pressure")
        self.diff_pressure_label.grid(
            column=self.label_col, row=row_num, sticky=tk.W)
        row_num += 1

        self.accel_x_label = ttk.Label(sensor_label_frame,
                                       text="Acceleration X")
        self.accel_x_label.grid(column=self.label_col,
                                row=row_num, sticky=tk.W)
        row_num += 1

        self.accel_y_label = ttk.Label(sensor_label_frame,
                                       text="Acceleration Y")
        self.accel_y_label.grid(column=self.label_col,
                                row=row_num, sticky=tk.W)
        row_num += 1

        self.accel_z_label = ttk.Label(sensor_label_frame,
                                       text="Acceleration Z")
        self.accel_z_label.grid(column=self.label_col,
                                row=row_num, sticky=tk.W)
        row_num += 1

        self.gyro_x_label = ttk.Label(sensor_label_frame,
                                      text="Gyroscope X")
        self.gyro_x_label.grid(column=self.label_col, row=row_num, sticky=tk.W)
        row_num += 1

        self.gyro_y_label = ttk.Label(sensor_label_frame,
                                      text="Gyroscope Y")
        self.gyro_y_label.grid(column=self.label_col, row=row_num, sticky=tk.W)
        row_num += 1

        self.gyro_z_label = ttk.Label(sensor_label_frame,
                                      text="Gyroscope Z")
        self.gyro_z_label.grid(column=self.label_col, row=row_num, sticky=tk.W)
        row_num += 1

        self.abs_temp_label = ttk.Label(sensor_label_frame,
                                        text="Absolute Sensor Temperature")
        self.abs_temp_label.grid(
            column=self.label_col, row=row_num, sticky=tk.W)
        row_num += 1

        self.diff_temp_label = ttk.Label(sensor_label_frame,
                                         text="Differential Sensor Temperature")
        self.diff_temp_label.grid(
            column=self.label_col, row=row_num, sticky=tk.W)
        row_num += 1

        self.accel_gyro_temperature_label = ttk.Label(sensor_label_frame,
                                                      text="Accelerometer/ Gyroscope Temperature")
        self.accel_gyro_temperature_label.grid(
            column=self.label_col, row=row_num, sticky=tk.W)
        row_num += 1

        self.temperature_label = ttk.Label(sensor_label_frame,
                                           text="Temperature Sensor")
        self.temperature_label.grid(
            column=self.label_col, row=row_num, sticky=tk.W)
        row_num += 1

    @classmethod
    def create_sensor_status(self, sensor_frame):
        sensor_status_frame = ttk.LabelFrame(sensor_frame, text="Status")
        sensor_status_frame.grid(column=self.status_col, row=0)

        # Populate the status elements
        row_num = 0

        self.abs_pressure_status = ttk.Label(sensor_status_frame,
                                             text=self.inital_status_text,
                                             style='warning.TLabel')
        self.abs_pressure_status.grid(column=0, row=row_num, sticky=tk.W)
        row_num += 1

        self.diff_pressure_status = ttk.Label(sensor_status_frame,
                                              text=self.inital_status_text,
                                              style='warning.TLabel')
        self.diff_pressure_status.grid(column=0, row=row_num, sticky=tk.W)
        row_num += 1

        self.accel_x_status = ttk.Label(sensor_status_frame,
                                        text=self.inital_status_text,
                                        style='warning.TLabel')
        self.accel_x_status.grid(column=0, row=row_num, sticky=tk.W)
        row_num += 1

        self.accel_y_status = ttk.Label(sensor_status_frame,
                                        text=self.inital_status_text,
                                        style='warning.TLabel')
        self.accel_y_status.grid(column=0, row=row_num, sticky=tk.W)
        row_num += 1

        self.accel_z_status = ttk.Label(sensor_status_frame,
                                        text=self.inital_status_text,
                                        style='warning.TLabel')
        self.accel_z_status.grid(column=0, row=row_num, sticky=tk.W)
        row_num += 1

        self.gyro_x_status = ttk.Label(sensor_status_frame,
                                       text=self.inital_status_text,
                                       style='warning.TLabel')
        self.gyro_x_status.grid(column=0, row=row_num, sticky=tk.W)
        row_num += 1

        self.gyro_y_status = ttk.Label(sensor_status_frame,
                                       text=self.inital_status_text,
                                       style='warning.TLabel')
        self.gyro_y_status.grid(column=0, row=row_num, sticky=tk.W)
        row_num += 1

        self.gyro_z_status = ttk.Label(sensor_status_frame,
                                       text=self.inital_status_text,
                                       style='warning.TLabel')
        self.gyro_z_status.grid(column=0, row=row_num, sticky=tk.W)
        row_num += 1

        self.abs_temp_status = ttk.Label(sensor_status_frame,
                                         text=self.inital_status_text,
                                         style='warning.TLabel')
        self.abs_temp_status.grid(column=0, row=row_num, sticky=tk.W)
        row_num += 1

        self.diff_temp_status = ttk.Label(sensor_status_frame,
                                          text=self.inital_status_text,
                                          style='warning.TLabel')
        self.diff_temp_status.grid(column=0, row=row_num, sticky=tk.W)
        row_num += 1

        self.accel_gyro_temperature_status = ttk.Label(
            sensor_status_frame, text=self.inital_status_text,
            style='warning.TLabel')
        self.accel_gyro_temperature_status.grid(
            column=0, row=row_num, sticky=tk.W)
        row_num += 1

        self.temperature_status = ttk.Label(sensor_status_frame,
                                            text=self.inital_status_text,
                                            style='warning.TLabel')
        self.temperature_status.grid(column=0, row=row_num, sticky=tk.W)
        row_num += 1

    @classmethod
    def create_sensor_values(self, sensor_frame):
        sensor_values_frame = ttk.LabelFrame(sensor_frame, text="Values")
        sensor_values_frame.grid(column=self.sensor_vals_col, row=0)

        # Populate the values elements
        row_num = 0

        self.abs_pressure_value = ttk.Label(
            sensor_values_frame,
            text=str(self.inital_values) + " KPa")
        self.abs_pressure_value.grid(column=0, row=row_num, sticky=tk.W)
        row_num += 1

        self.diff_pressure_value = ttk.Label(
            sensor_values_frame,
            text=str(self.inital_values) + " Pa")
        self.diff_pressure_value.grid(column=0, row=row_num, sticky=tk.W)
        row_num += 1

        self.accel_x_value = ttk.Label(sensor_values_frame,
                                        text=str(self.inital_values) + " m/s^2")
        self.accel_x_value.grid(column=0, row=row_num, sticky=tk.W)
        row_num += 1

        self.accel_y_value = ttk.Label(sensor_values_frame,
                                        text=str(self.inital_values) + " m/s^2")
        self.accel_y_value.grid(column=0, row=row_num, sticky=tk.W)
        row_num += 1

        self.accel_z_value = ttk.Label(sensor_values_frame,
                                        text=str(self.inital_values) + " m/s^2")
        self.accel_z_value.grid(column=0, row=row_num, sticky=tk.W)
        row_num += 1

        self.gyro_x_value = ttk.Label(sensor_values_frame,
                                        text=str(self.inital_values) + " deg/s")
        self.gyro_x_value.grid(column=0, row=row_num, sticky=tk.W)
        row_num += 1

        self.gyro_y_value = ttk.Label(sensor_values_frame,
                                        text=str(self.inital_values) + " deg/s")
        self.gyro_y_value.grid(column=0, row=row_num, sticky=tk.W)
        row_num += 1

        self.gyro_z_value = ttk.Label(sensor_values_frame,
                                        text=str(self.inital_values) + " deg/s")
        self.gyro_z_value.grid(column=0, row=row_num, sticky=tk.W)
        row_num += 1

        self.abs_temp_value = ttk.Label(
            sensor_values_frame,
            text=str(self.inital_values) + " " + self.degree_symbol + "C")
        self.abs_temp_value.grid(column=0, row=row_num, sticky=tk.W)
        row_num += 1

        self.diff_temp_value = ttk.Label(
            sensor_values_frame,
            text=str(self.inital_values) + " " + self.degree_symbol + "C")
        self.diff_temp_value.grid(column=0, row=row_num, sticky=tk.W)
        row_num += 1

        self.accel_gyro_temperature_value = ttk.Label(
            sensor_values_frame,
            text=str(self.inital_values) + " " + self.degree_symbol + "C")
        self.accel_gyro_temperature_value.grid(
            column=0, row=row_num, sticky=tk.W)
        row_num += 1

        self.temperature_value = ttk.Label(
            sensor_values_frame,
            text=str(self.inital_values) + " " + self.degree_symbol + "C")
        self.temperature_value.grid(column=0, row=row_num, sticky=tk.W)
        row_num += 1

    @classmethod
    def stop_all_motors(self):
        self.stop_thruster()
        self.stop_rudder()
        self.stop_elevator()
        self.bouyancy_hold()

    @classmethod
    def thruster_slider(self, val):
        self.thruster_set.delete(0, END)
        self.thruster_set.insert(0, val)
        self.commands.thruster = float(val)

    @classmethod
    def elevator_slider(self, val):
        self.elevator_set.delete(0, END)
        self.elevator_set.insert(0, val)
        self.commands.elevator = float(val)

    @classmethod
    def rudder_slider(self, val):
        self.rudder_set.delete(0, END)
        self.rudder_set.insert(0, val)
        self.commands.rudder = float(val)

    @classmethod
    def stop_thruster(self):
        self.thruster_set.delete(0, END)
        self.thruster_set.insert(0, "0")
        self.thruster_set_change(0)

    @classmethod
    def stop_elevator(self):
        self.elevator_set.delete(0, END)
        self.elevator_set.insert(0, "0")
        self.elevator_set_change(0)

    @classmethod
    def stop_rudder(self):
        self.rudder_set.delete(0, END)
        self.rudder_set.insert(0, "0")
        self.rudder_set_change(0)

    @classmethod
    def rudder_diff(self):
        self.rudder_together_button.config(relief='raised')
        self.rudder_diff_button.config(relief='sunken')
        self.commands.rudder_drive = Rudder_Drive_Direction.DIFF

    @classmethod
    def rudder_together(self):
        self.rudder_together_button.config(relief='sunken')
        self.rudder_diff_button.config(relief='raised')
        self.commands.rudder_drive = Rudder_Drive_Direction.TOGETHER

    @classmethod
    def bouyancy_hold(self):
        self.bouyancy_hold_button.config(relief='sunken')
        self.bouyancy_up_button.config(relief='raised')
        self.bouyancy_down_button.config(relief='raised')
        self.commands.bouyancy = Bouyancy_Direction.HOLD

    @classmethod
    def bouyancy_up(self):
        self.bouyancy_hold_button.config(relief='raised')
        self.bouyancy_up_button.config(relief='sunken')
        self.bouyancy_down_button.config(relief='raised')
        self.commands.bouyancy = Bouyancy_Direction.UP

    @classmethod
    def bouyancy_down(self):
        self.bouyancy_hold_button.config(relief='raised')
        self.bouyancy_up_button.config(relief='raised')
        self.bouyancy_down_button.config(relief='sunken')
        self.commands.bouyancy = Bouyancy_Direction.DOWN

    @classmethod
    def thruster_set_change(self, nop):
        val = self.thruster_set.get()
        self.thruster_slider.set(val)
        self.commands.thruster = float(val)

    @classmethod
    def elevator_set_change(self, nop):
        val = self.elevator_set.get()
        self.elevator_slider.set(val)
        self.commands.elevator = float(val)

    @classmethod
    def rudder_set_change(self, nop):
        val = self.rudder_set.get()
        self.rudder_slider.set(val)
        self.commands.rudder = float(val)

    @classmethod
    def update_values(self, vals):
        self.abs_pressure_value.config(
            text=str(vals.abs_pressure)+" KPa")
        self.diff_pressure_value.config(
            text=str(vals.diff_pressure) + " Pa")
        self.abs_temp_value.config(
            text=str(vals.abs_temperature) + " " + self.degree_symbol + "C")
        self.diff_temp_value.config(
            text=str(vals.diff_temperature) + " " + self.degree_symbol + "C")
        self.temperature_value.config(
            text=str(vals.temperature) + " " + self.degree_symbol + "C")
        self.estimated_volume_entry.config(
            text=str(vals.bouyancy_volume) + " %")
        self.accel_x_value.config(
            text=str(vals.accel_x) + " m/s^2")
        self.accel_y_value.config(
            text=str(vals.accel_y) + " m/s^2")
        self.accel_z_value.config(
            text=str(vals.accel_z) + " m/s^2")
        self.gyro_x_value.config(
            text=str(vals.gyro_x) + " deg/s")
        self.gyro_y_value.config(
            text=str(vals.gyro_y) + " deg/s")
        self.gyro_z_value.config(
            text=str(vals.gyro_z) + " deg/s")
        self.accel_gyro_temperature_value.config(
            text=str(vals.accel_gyro_temperature) + " " + self.degree_symbol + "C")

    @classmethod
    def update_status(self, status):
        self.thruster_status.config(text=status.thruster.name)
        self.elevator_status.config(text=status.elevator.name)
        self.rudder_status.config(text=status.rudder.name)
        self.abs_pressure_status.config(text=status.abs_pressure.name)
        self.diff_pressure_status.config(text=status.diff_pressure.name)
        self.abs_temp_status.config(text=status.abs_pressure.name)
        self.diff_temp_status.config(text=status.diff_pressure.name)
        self.temperature_status.config(text=status.temperature.name)
        self.accel_x_status.config(text=status.accel_gyro.name)
        self.accel_y_status.config(text=status.accel_gyro.name)
        self.accel_z_status.config(text=status.accel_gyro.name)
        self.gyro_x_status.config(text=status.accel_gyro.name)
        self.gyro_y_status.config(text=status.accel_gyro.name)
        self.gyro_z_status.config(text=status.accel_gyro.name)
        self.accel_gyro_temperature_status.config(text=status.accel_gyro.name)

        if status.thruster == States.OK:
            self.thruster_status.config(style='ok.TLabel')
        elif status.thruster == States.ERROR:
            self.thruster_status.config(style='error.TLabel')
        else:
            self.thruster_status.config(style='warning.TLabel')

        if status.elevator == States.OK:
            self.elevator_status.config(style='ok.TLabel')
        elif status.elevator == States.ERROR:
            self.elevator_status.config(style='error.TLabel')
        else:
            self.elevator_status.config(style='warning.TLabel')

        if status.rudder == States.OK:
            self.rudder_status.config(style='ok.TLabel')
        elif status.rudder == States.ERROR:
            self.rudder_status.config(style='error.TLabel')
        else:
            self.rudder_status.config(style='warning.TLabel')

        if status.abs_pressure == States.OK:
            self.abs_pressure_status.config(style='ok.TLabel')
            self.abs_temp_status.config(style='ok.TLabel')
        elif status.abs_pressure == States.ERROR:
            self.abs_pressure_status.config(style='error.TLabel')
            self.abs_temp_status.config(style='error.TLabel')
        else:
            self.abs_pressure_status.config(style='warning.TLabel')
            self.abs_temp_status.config(style='warning.TLabel')

        if status.diff_pressure == States.OK:
            self.diff_pressure_status.config(style='ok.TLabel')
            self.diff_temp_status.config(style='ok.TLabel')
        elif status.diff_pressure == States.ERROR:
            self.diff_pressure_status.config(style='error.TLabel')
            self.diff_temp_status.config(style='error.TLabel')
        else:
            self.diff_pressure_status.config(style='warning.TLabel')
            self.diff_temp_status.config(style='warning.TLabel')

        if status.temperature == States.OK:
            self.temperature_status.config(style='ok.TLabel')
        elif status.temperature == States.ERROR:
            self.temperature_status.config(style='error.TLabel')
        else:
            self.temperature_status.config(style='warning.TLabel')

        if status.accel_gyro == States.OK:
            self.accel_x_status.config(style='ok.TLabel')
            self.accel_y_status.config(style='ok.TLabel')
            self.accel_z_status.config(style='ok.TLabel')
            self.gyro_x_status.config(style='ok.TLabel')
            self.gyro_y_status.config(style='ok.TLabel')
            self.gyro_z_status.config(style='ok.TLabel')
            self.accel_gyro_temperature_status.config(style='ok.TLabel')
        elif status.accel_gyro == States.ERROR:
            self.accel_x_status.config(style='error.TLabel')
            self.accel_y_status.config(style='error.TLabel')
            self.accel_z_status.config(style='error.TLabel')
            self.gyro_x_status.config(style='error.TLabel')
            self.gyro_y_status.config(style='error.TLabel')
            self.gyro_z_status.config(style='error.TLabel')
            self.accel_gyro_temperature_status.config(style='error.TLabel')
        else:
            self.accel_x_status.config(style='warning.TLabel')
            self.accel_y_status.config(style='warning.TLabel')
            self.accel_z_status.config(style='warning.TLabel')
            self.gyro_x_status.config(style='warning.TLabel')
            self.gyro_y_status.config(style='warning.TLabel')
            self.gyro_z_status.config(style='warning.TLabel')
            self.accel_gyro_temperature_status.config(style='warning.TLabel')

    @classmethod
    def update_connection(self, connection):
        self.connection_status.config(text=connection.name)

        if connection == Connection_Status.CONNECTED:
            self.connection_status.config(style='ok.TLabel')
        else:
            self.connection_status.config(style='error.TLabel')

    @classmethod
    def close(self):
        self.win.quit()
