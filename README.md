# urov-gui

## Name
UROV GUI Application

## Description
This repo is a GUI interface for the UROV capstone project. Firmware for that project can be found here: https://gitlab.com/MeganHolmes/urov-firmware

## Authors and acknowledgment
Software & Firmware by Megan Holmes, Power Distribution and Sensors by Morgan Buck, Hull Design and Hydrodynamics by Zoey Van Rassel, Bouyancy Tank, Internal Layout, and Material Selection by Brock Myers
