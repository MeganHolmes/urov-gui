
from enum import Enum

class Rudder_Drive_Direction(Enum):
    TOGETHER = 0
    DIFF = 1

class Bouyancy_Direction(Enum):
    HOLD = 0
    UP = 1
    DOWN = 2

class States(Enum):
    OK = 0
    ERROR = 1
    UNKNOWN = 2

class Connection_Status(Enum):
    CONNECTED = 0
    DISCONNECTED = 1

class Commands:
    thruster = 0
    elevator = 0
    rudder = 0
    rudder_drive = Rudder_Drive_Direction.TOGETHER
    bouyancy = Bouyancy_Direction.HOLD

class Values:
    bouyancy_volume = 0.0
    abs_pressure = 0.0
    abs_temperature = 0.0
    diff_pressure = 0.0
    diff_temperature = 0.0
    temperature = 0.0
    accel_x = 0.0
    accel_y = 0.0
    accel_z = 0.0
    gyro_x = 0.0
    gyro_y = 0.0
    gyro_z = 0.0
    accel_gyro_temperature = 0.0

class Component_States:
    thruster = States.UNKNOWN
    rudder = States.UNKNOWN
    elevator = States.UNKNOWN
    abs_pressure = States.UNKNOWN
    diff_pressure = States.UNKNOWN
    temperature = States.UNKNOWN
    accel_gyro = States.UNKNOWN
