

# System Imports
import os
import can
from threading import Thread
from time import sleep
import time

# Project Imports
from gui import GUI
from structures import *

def main():
    # Init base data structures
    gui_commands = Commands
    vals = Values
    states = Component_States

    # Init USB / CAN
    can0 = init_can()

    if can0 is not None:
        # Init GUI
        gui = GUI
        gui_thread = Thread(target=gui.start, args=(gui_commands,))
        gui_thread.start()
        sleep(1)  # Wait for GUI to start
        last_tx_time = 0
        last_rx_time = 0
        disconnect_wait_time = 1
        connected_flag = False

        while True:

            # Get data from USB / CAN
            can_msg = rx_can(can0)

            if can_msg is not None:
                if can_msg.arbitration_id == 0x020: # This is the UROV's Id

                    vals, states = translate_recv_data(can_msg, vals, states)

                    # Update GUI
                    gui.update_connection(Connection_Status.CONNECTED)
                    gui.update_values(vals)
                    gui.update_status(states)

                    # Want a printout the first time a packet is received
                    if last_rx_time == 0:
                        print("UROV CONNECTED")
                        states.thruster = States.OK
                        states.elevator = States.OK
                        states.rudder = States.OK

                    connected_flag = True
                    last_rx_time = time.time()
                else:
                    if time.time() - last_rx_time > disconnect_wait_time:
                        connected_flag = False
            else:
                if time.time() - last_rx_time > disconnect_wait_time:
                    connected_flag = False

            # Send commands to USB / CAN
            if connected_flag:
                if time.time() - last_tx_time > 0.25:

                    # Check for commands from GUI
                    data = format_commands_for_tx(gui_commands)

                    tx_can(can0, data)
                    last_tx_time = time.time()
            else:
                gui.update_connection(Connection_Status.DISCONNECTED)

            # Make sure GUI is still running
            if not gui_thread.is_alive():
                print("GUI has closed")
                break

        # Close CAN and GUI
        emerg_surface(can0)
        shutdown(gui)


def init_can():
    os.system('sudo ifconfig can0 down')
    os.system('sudo ip link set can0 type can bitrate 31250')
    os.system('sudo ifconfig can0 up')

    can0 = None

    try:
        can0 = can.interface.Bus(
            channel='can0', bustype='socketcan', bitrate=31250)
    except:
        print("CAN failed to initialize, aborting")
        shutdown(None)

    sleep(1)
    return can0


def tx_can(can0, data):
    try:
        msg = can.Message(timestamp=0,
            arbitration_id=0x7E0,
                      data=data,
                      is_extended_id=False,
                      is_remote_frame=False,
                      is_error_frame=False,
                      channel=None,
                      dlc=8,
                      is_fd=False,
                      is_rx=False,
                      bitrate_switch=False,
                      error_state_indicator=False,
                      check=False)

        can0.send(msg, timeout=0.0)
    except:
        # The most likely cause of this is a buffer overflow
        print("Tx CAN failed")
        shutdown(None)


def rx_can(can0):
    return can0.recv(0.01) # Seconds to wait for a message to be received

def shutdown(gui):
    os.system('sudo ifconfig can0 down')

    if gui is not None:
        gui.close()

    sleep(1)


def format_commands_for_tx(commands):
    data = []
    scale = 65535
    max_thrust = 100
    max_angle = 45

    # -100% = 0, 0% = scale/2, 100% = scale
    thrust = int(((commands.thruster + max_thrust)/(max_thrust*2))*scale)
    elevator = int(((commands.elevator + max_angle)/(max_angle*2))*scale)
    rudder = int(((commands.rudder + max_angle)/(max_angle*2))*scale)

    # Breaks the data into two bytes
    data.append(thrust >> 8)
    data.append(thrust & 0xFF)

    data.append(elevator >> 8)
    data.append(elevator & 0xFF)

    data.append(rudder >> 8)
    data.append(rudder & 0xFF)

    # If more data is added later and we need to save a byte we can combine
    # these two into one since we only need 3 bits.
    data.append(commands.rudder_drive.value)
    data.append(commands.bouyancy.value)

    return data


def emerg_surface(can0):
    zero_point = int(65535/2)
    zero_point_msb = zero_point >> 8
    zero_point_lsb = zero_point & 0xFF

    data = [zero_point_msb, zero_point_lsb,
            zero_point_msb, zero_point_lsb,
            zero_point_msb, zero_point_lsb,
            0, Bouyancy_Direction.UP.value]

    tx_can(can0, data)

def translate_recv_data(msg, vals, states):

    if msg.dlc != 2:
        # First byte is the packet ID
        if msg.data[0] == 0:
            states = Component_States
            states.abs_pressure = States(msg.data[1])
            states.diff_pressure = States(msg.data[2])
            states.temperature = States(msg.data[3])
            states.accel_gyro = States(msg.data[4])
            vals.abs_pressure = round(translate_to_float(msg.data[5:8]),2)
        elif msg.data[0] == 1:
            vals.diff_pressure = round(translate_to_float(msg.data[1:4]),2)
            vals.temperature = round(translate_to_float(msg.data[4:7]),2)
        elif msg.data[0] == 2:
            vals.abs_temperature = translate_to_float(msg.data[1:4])
            vals.diff_temperature = translate_to_float(msg.data[4:7])
        elif msg.data[0] == 3:
            vals.bouyancy_volume = round(translate_to_float(msg.data[1:4]),1)
            vals.accel_gyro_temperature = translate_to_float(msg.data[4:7])
        elif msg.data[0] == 4:
            vals.accel_x = round(translate_to_float(msg.data[1:4]),2)
            vals.accel_y = round(translate_to_float(msg.data[4:7]),2)
        elif msg.data[0] == 5:
            vals.accel_z = round(translate_to_float(msg.data[1:4]),2)
            vals.gyro_x = round(translate_to_float(msg.data[4:7]),2)
        elif msg.data[0] == 6:
            vals.gyro_y = round(translate_to_float(msg.data[1:4]),2)
            vals.gyro_z = round(translate_to_float(msg.data[4:7]),2)
        else:
            print("Unknown packet received")
    else:
        code = (msg.data[0] << 8) | (msg.data[1])

        if code == 0:
            print("Error Code 0: Init All Sensors ERROR")
        elif code == 1:
            print("Error Code 1: UROV Recieve Error. UROV can't hear us")
        elif code == 2:
            print("Error Code 2: Motor Error")
            states.thruster = States.ERROR
            states.elevator = States.ERROR
            states.rudder = States.ERROR
        elif code == 3:
            print("Error Code 3: Read All Sensor Error")
        elif code == 4:
            print("Error Code 4: UROV Data Transmit Error")
        elif code == 5:
            print("Error Code 5: Reinit Sensor Error")
        elif code == 6:
            print("Error Code 6: No Connection Error. UROV hasn't heard from us.")
        elif code == 7:
            print("Error Code 7: Tank Timer Overflow")

    return vals, states

def translate_to_float(fl):
    # byte 1 = exponent. first bit is sign
    # byte 2 = upper 8 bits. first bit is sign
    # byte 3 = lower 8 bits

    exp_sign = int(fl[0] >> 7)
    mantissa_sign = int(fl[1] >> 7)

    exponent = int(fl[0] & (0x7F))*(-1)**exp_sign
    upper = (int(fl[1] & 0x7F) << 8)
    lower = int(fl[2])
    combined = ((upper | lower)/32767)*(-1)**mantissa_sign


    return combined * (2**exponent)



if __name__ == "__main__":
    main()
